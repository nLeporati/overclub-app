import { useState } from 'react';
import { IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { ChatMessage } from './ChatMessage';
import { chatboxEllipses } from 'ionicons/icons';
import './Messages.css';
import { useHistory } from 'react-router';

export const mockChats = [
  {
    id: 'general',
    name: 'Acción Fútbol',
    picture: 'https://randomuser.me/api/portraits/thumb/men/6.jpg',
    lastMessage: {
      text: 'Bienvenidos a Acción Fútbol, acá compartiremo',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'group',
    group: 'General'
  },
  {
    id: 'adultos-am',
    name: 'Adultos AM',
    picture: 'https://randomuser.me/api/portraits/thumb/men/34.jpg',
    lastMessage: {
      text: 'Recuerden ver el juego del sábado',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'group',
    group: 'Adultos AM'
  },
  {
    id: 'adultos-am.martes-y-jueves',
    name: 'Martes y Jueves',
    picture: 'https://randomuser.me/api/portraits/thumb/men/2.jpg',
    lastMessage: {
      text: 'Todxs! mañana se suspende por lluvias!',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'team',
    group: 'Adultos AM'
  },
  {
    id: 'adultos-am.lunes-y-miercoles',
    name: 'Lunes y Miercoles',
    picture: 'https://randomuser.me/api/portraits/thumb/men/1.jpg',
    lastMessage: {
      text: 'Mañana la clase comienza a las 7:00 AM',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'team',
    group: 'Adultos AM'
  },
  {
    id: 'gol-kids',
    name: 'GOL KIDS',
    picture: 'https://randomuser.me/api/portraits/thumb/men/5.jpg',
    lastMessage: {
      text: 'Subo la clase de mañana',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'group',
    group: 'GOL KIDS'
  },
  {
    id: 'Afd34dfDg',
    name: 'Asado el Sábado :)',
    picture: 'https://randomuser.me/api/portraits/thumb/men/4.jpg',
    lastMessage: {
      text: 'Yo llevo las bebidas!',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'custom'
  },
  {
    id: 'miguel',
    name: 'Miguel',
    picture: 'https://randomuser.me/api/portraits/thumb/men/63.jpg',
    lastMessage: {
      text: 'Hola! como has estado?',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'direct'
  },
  {
    id: 'alberto',
    name: 'Alberto',
    picture: 'https://randomuser.me/api/portraits/thumb/men/24.jpg',
    lastMessage: {
      text: 'Profe mañana hay clases?',
      date: 'March 13, 08 04:20',
      sender: 'Miguel',
      type: 'text'
    },
    type: 'direct'
  },
  {
    id: 'daniela',
    name: 'Daniela',
    picture: 'https://randomuser.me/api/portraits/thumb/women/24.jpg',
    type: 'direct'
  },
]

export const Messages: React.FC = () => {
  const [chats, setChats] = useState(mockChats);
  const history = useHistory();

  // useEffect(() => {
  //   fetch('https://randomuser.me/api/?results=20')
  //     .then(resp => resp.json())
  //     .then(resp => setChats(resp.results))
  // }, [])

  const newMessage = () => {
    history.push('/chat-new');
  } 
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Messages</IonTitle>
          <IonButtons slot="end">
              <IonButton onClick={newMessage}>
                <IonIcon slot="icon-only" icon={chatboxEllipses} />
              </IonButton>
            </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>

        {chats.map((chat: any) =>
          (<ChatMessage
            chat={chat}
            key={chat.id}
          />)
        )}
      </IonContent>
    </IonPage>
  );
};
