import React, { useState } from 'react';
import { IonAvatar, IonBackButton, IonButton, IonButtons, IonCheckbox, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, search } from 'ionicons/icons';
import { mockChats } from './Messages';
import { useHistory } from 'react-router';


export const NewMessage: React.FC = () => {
  const history = useHistory();
  const [selected, setSelected] = useState<any[]>(['me']);

  const onSelect = (e: CustomEvent) => {
     if (e.detail.checked) {
      setSelected(prev => [...prev, e.detail.value])
    } else {
      const exist = selected.find(s => s === e.detail.value)
      if (exist) {
        setSelected(prev => prev.filter(s => s !== e.detail.value))
      }
    }
  }

  const onContinue = () => {
    if (selected.length <= 1) return

    const id = selected
      .filter(s => s !== 'me').map((s: string) => s.toLowerCase())
      .join('-')

    const name = selected
      .filter(s => s !== 'me')
      .map((s: string) => s[0].toUpperCase()+s.slice(1, s.length))
      .join(', ')
      
    const newChat = {
      id,
      name,
      picture: 'https://randomuser.me/api/portraits/thumb/men/44.jpg',
      type: selected.length === 2 ? 'direct' : 'group'
    }    

    if (!mockChats.find(ch => ch.id === newChat.id)) {
      mockChats.push(newChat)
    }
    history.replace(`chat/${newChat.id}`);
    setSelected([]);
  }
  
  return(
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/chat"/>
          </IonButtons>
          <IonTitle>New Message</IonTitle>
          <IonButtons slot="end">
            <IonButton>
              <IonIcon slot="icon-only" icon={search} />
            </IonButton>
            <IonButton onClick={onContinue}>
              <IonIcon slot="icon-only" icon={arrowForward} />
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>

        {/* <IonItem lines="none">
            <IonButtons slot="start">
              <IonToggle color="secondary" checked={newGroup} onIonChange={e => setNewGroup(e.detail.checked)}/>
            </IonButtons>
          <IonLabel>
            New Group
          </IonLabel>
        </IonItem> */}
        <IonList lines="none">
            {mockChats.filter(ch => ch.type === 'direct').map((chat: any) =>(
              <IonItem key={chat.id}>
                <IonAvatar slot="start">
                  <img src={chat.picture} alt={chat.name} />
                </IonAvatar>
                <IonLabel>
                  <h2>{chat.name}</h2>
                </IonLabel>
                <IonCheckbox
                  slot="end"
                  value={chat.id}
                  color="secondary"
                  onIonChange={onSelect}
                  checked={selected.find(s => s === chat.id)}
                />
              </IonItem>
            ))}
        </IonList>
      </IonContent>
    </IonPage>
  )
}
