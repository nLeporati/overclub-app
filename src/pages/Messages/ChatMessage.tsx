import { IonAvatar, IonBadge, IonItem, IonLabel } from "@ionic/react"
import { IMessage } from "pages/Chat/Message";
import { useHistory } from 'react-router-dom'
import dayjs from 'dayjs';

type ChatMessageProps = {
  chat: {
    id: string;
    picture: string;
    name: string;
    lastMessage?: IMessage;
    group?: string;
    type: 'direct' | 'team' | 'group' | 'custom' | 'general'
  }
}

export const ChatMessage: React.FC<ChatMessageProps> = ({chat}) => {
  const history = useHistory();

  return (
    <IonItem
      button
      onClick={() => history.push(`chat/${chat.id}`)}
    >
      <IonAvatar slot="start">
        <img src={chat.picture} alt={chat.name} />
      </IonAvatar>
      <IonLabel>
        {chat.id === 'general' && <IonBadge color="success">General</IonBadge>}
        {" "}{chat.group && chat.id !== 'general' && <IonBadge color="secondary">{chat.group}</IonBadge>}
        {" "}{chat.type === 'team' && <IonBadge color="tertiary">Team</IonBadge>}
        {" "}{chat.type === 'custom' && <IonBadge color="medium">Group</IonBadge>}
        <div className="message-title">
          <h2>{chat.name}</h2>
          {chat.lastMessage && <small>
            {dayjs(chat.lastMessage.date).format('HH:mm')}
          </small>}
          </div>
        <p>{chat.lastMessage?.text}</p>
      </IonLabel>
    </IonItem>
  )
}
