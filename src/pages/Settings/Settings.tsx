import { IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { clipboardOutline, copyOutline, information, layersOutline, peopleOutline, settingsOutline, shirtOutline } from 'ionicons/icons';
import { useHistory } from 'react-router';

export const Settings: React.FC = () => {
  const history = useHistory();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonTitle>Settings</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Settings</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList lines="full">
          <IonItem detail button onClick={() => history.push('club-info')}>
            <IonIcon size="large" slot="start" icon={information} />
            <IonLabel>Club Information</IonLabel>
          </IonItem>
          <IonItem detail button onClick={() => history.push('users')}>
            <IonIcon size="large" slot="start" icon={peopleOutline} />
            <IonLabel>Users</IonLabel>
          </IonItem>
          <IonItem detail href="/">
            <IonIcon size="large" slot="start" icon={clipboardOutline} />
            <IonLabel>Groups</IonLabel>
          </IonItem>
          <IonItem detail href="/">
            <IonIcon size="large" slot="start" icon={shirtOutline} />
            <IonLabel>Teams</IonLabel>
          </IonItem>
          <IonItem detail href="/">
            <IonIcon size="large" slot="start" icon={layersOutline} />
            <IonLabel>Forms</IonLabel>
          </IonItem>
          <IonItem detail href="/">
            <IonIcon size="large" slot="start" icon={settingsOutline} />
            <IonLabel>App Preferences</IonLabel>
          </IonItem>
        </IonList>
      </IonContent>
    </IonPage>
  );
};
