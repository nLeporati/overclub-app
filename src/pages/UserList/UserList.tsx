import { useEffect, useState } from "react";
import { IonAvatar, IonBackButton, IonBadge, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonItem, IonLabel, IonList, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react"
import { useHistory } from "react-router";

export const UserList = () => {
  const [users, setUsers] = useState([]);
  const history = useHistory();

  useEffect(() => {
    fetch('https://randomuser.me/api/?results=20')
      .then(resp => resp.json())
      .then(resp => setUsers(resp.results))
  }, [])

  return(
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/settings"/>
          </IonButtons>
          <IonTitle>Users</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
          {users.map((user: any, i: number) => (
            <IonItem key={i} button onClick={() => history.push('/users/1234')}>
              <IonAvatar slot="start">
                <img src={user.picture.thumbnail} alt={user.name.first} />
              </IonAvatar>
              <IonLabel>
                <IonBadge color="secondary">Adulto AM</IonBadge>
                {/* {" "}<IonBadge color="tertiary">Jueves y Viernes</IonBadge> */}
                <h2>{user.name.first}{' '}{user.name.last}</h2>
              </IonLabel>
            </IonItem>
          ))}
        </IonList>
      </IonContent>
    </IonPage>
  )
}
