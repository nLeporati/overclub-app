import React from "react";
import dayjs from 'dayjs';

export interface IMessage {
  text: string,
  date: string,
  sender: string,
  type: 'text'
}

type MessageProps = {
  message: IMessage;
  group?: string;
}

export const Message: React.FC<MessageProps> = ({message, group}) => {
  const isMe = message.sender === 'me';
  const time = dayjs(message.date).format('HH:mm');  

  return(
    <div className="message"
      style={{
        alignSelf: isMe ? 'flex-end' : 'flex-start',
        background: isMe ? 'lightblue' : 'var(--ion-color-light)',
      }}
    >
      {group && !isMe && <strong>{message.sender}</strong>}

      <p>{message.text}</p>

      <div style={{textAlign: isMe ? 'end' : 'start'}}>
        <small>{time}</small>
      </div>
    </div>
  )
}
