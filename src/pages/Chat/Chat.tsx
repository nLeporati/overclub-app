import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useHistory } from 'react-router';
import {
  IonAvatar,
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonPage,
  IonRow,
  IonTextarea,
  IonTitle,
  IonToolbar
} from '@ionic/react';
import { ellipsisVertical, send } from 'ionicons/icons';
import './Chat.css';
import { IMessage } from './Message';
import { RenderMessages } from './RenderMessages';
import Pusher from 'pusher-js';
import dayjs from 'dayjs';
import { mockChats } from 'pages/Messages/Messages';

const msgs: IMessage[] = [
  {
    text: 'Hola!',
    date: 'March 13, 08 04:20',
    sender: 'Miguel',
    type: 'text'
  },
  {
    text: 'como estas?',
    date: 'March 13, 08 04:20',
    sender: 'Miguel',
    type: 'text'
  },
  {
    text: 'yo muy bien, estoy avanzando en la creación de la app de deportes, en este momento haciendo el chat!',
    date: 'March 13, 08 04:20',
    sender: 'Miguel',
    type: 'text'
  },
]

const club = 'accionfutbol';

export const Chat: React.FC = () => {
  const route = useHistory();
  const params = useParams<{id: string}>();
  const [messages, setMessages] = useState<IMessage[]>(msgs)
  const [inputText, setInputText] = useState('')
  const [chat, setChat] = useState<any | undefined>();

  function scrollBottom() {
    const el = document.getElementsByClassName('messages')[0];
    el.scrollTop = el.scrollHeight;
  }

  useEffect(() => {
    const chat = mockChats.find(ch => ch.id === params.id);
    setChat(chat)
    
    const chatChannel = [club, params.id].join('.');
    const pusher = new Pusher('b5e75b3e29df2566ee62', {
      cluster: 'us2'
    });

    var channel = pusher.subscribe(chatChannel);
    channel.bind('chat.send-message', function(message: IMessage) {
      setMessages((lasts) => [...lasts, message]);
      scrollBottom();
    });

    return () => {
      pusher.unbind_all()
      pusher.unsubscribe(chatChannel)
      return;
    };
  }, []);

  const sendMessage = () => {
    if (inputText.trim() === '') return;

    const message: IMessage = {
      text: inputText.trim(),
      date: dayjs().toString(),
      sender: 'me',
      type: 'text'
    };
    setMessages((lasts) => [...lasts, message]);
    setInputText('');
    scrollBottom();
  }

  function goToUser() {
    if (chat.type === 'direct') {
      route.push(`/users/${chat.id}`)
    }
  }

  const getTitle = (chat: any) => {
    return(
      <IonTitle class="ion-no-padding" onClick={goToUser}>
        {chat.name !== chat.group && chat.id !== 'general' && (
          <div>
            <sub>{chat.group}</sub>
          </div>
        )}
        <h2 className="ion-no-margin">{chat.name}</h2>
      </IonTitle>
    )
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/chat"/>
          </IonButtons>
          {chat && (
            <IonAvatar slot="start">
              <img style={{padding: '0.5rem'}} src={chat.picture} alt={chat.name} />
            </IonAvatar>
          )}
          {chat && getTitle(chat)}
          <IonButtons slot="primary">
              <IonButton>
                <IonIcon slot="icon-only" icon={ellipsisVertical} />
              </IonButton>
            </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <RenderMessages messages={messages} />

        <div className="inputBox">
          <div style={{width: '90%'}}>
            <IonRow>
              <IonCol class="ion-no-padding">
                  {/* <IonTextarea
                    class="ion-no-margin"
                    autofocus
                    placeholder="Writte a message"
                    enterkeyhint="send"
                    inputMode="text"
                    value={inputText}
                    onIonChange={(e) => setInputText(String(e.detail.value))}
                    /> */}
                  <IonInput
                    class="ion-no-margin"
                    autofocus
                    placeholder="Writte a message"
                    enterkeyhint="send"
                    inputMode="text"
                    value={inputText}
                    onIonChange={(e) => setInputText(String(e.detail.value))}
                    />
              </IonCol>
            </IonRow>
          </div>
          <div className="send-button">
            <IonButtons slot="icon-only">
              <IonButton onClick={sendMessage}>
                <IonIcon slot="icon-only" color="primary" icon={send}/>
              </IonButton>
            </IonButtons>
          </div>
        </div>
      </IonContent>
    </IonPage>
  );
};
