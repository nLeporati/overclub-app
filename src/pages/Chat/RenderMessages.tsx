import React from "react";
import { IMessage, Message } from "./Message";
import dayjs from 'dayjs';

export const RenderMessages: React.FC<{messages: IMessage[]}> = ({messages}) => {
  function hasShowDate(msg: IMessage, i: number) {
    const prevMsg = messages[i-1];
    const date = dayjs(msg.date);

    return !prevMsg || (prevMsg && dayjs(prevMsg.date).day() !== date.day())
  }

  function getDate(msg: IMessage) {
    return dayjs(msg.date).format('MMM DD, YYYY');
  }

  return(
    <div className="messages">
      {messages.map((msg, i) => {
        if (hasShowDate(msg, i)) {
          return(
            <React.Fragment key={i}>
              <small className="ion-margin" style={{textAlign: 'center'}}>{getDate(msg)}</small>
              <Message message={msg} />
            </React.Fragment>
          )
        } else {
          return <Message key={i} message={msg} />
        }
      })
    }
  </div>
  )
}