import { useEffect, useState } from "react";
import { IonAvatar, IonBackButton, IonBadge, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonItem, IonLabel, IonList, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react"
import { useHistory } from "react-router";

export const UserDetails = () => {

  return(
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/settings"/>
          </IonButtons>
          <IonTitle>Sandro Wells</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonList>
        </IonList>
      </IonContent>
    </IonPage>
  )
}