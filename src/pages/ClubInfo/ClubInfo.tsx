import { IonBackButton, IonButtons, IonCol, IonContent, IonGrid, IonHeader, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react"
import logo from '../../assets/images/club-logo.png';

export const ClubInfo = () => {
  return(
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/settings"/>
          </IonButtons>
          <IonTitle>Club Information</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonGrid>
        <IonRow>
          <IonCol className="ion-text-center">
            <small>nombre</small>
            <h1 className="ion-no-margin">ACCIÓN FÚTBOL</h1>
            <div style={{height: '1rem'}}></div>       
            <img style={{height: '20rem'}} src={logo} alt="club logo"/>
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol className="ion-text-center">
            <small>resumen</small>
            <h5 style={{textAlign: 'justify'}} className="ion-no-margin">
              Acción Fútbol es un club donde cualquiera puede entrenar como profesional, levantandose a la 6 AM!
            </h5>         
          </IonCol>
        </IonRow>
        <IonRow>
          <IonCol className="ion-text-center">
            <small>administrador</small>
            <p className="ion-text-left ion-no-margin"><strong>Nombre</strong>: Ignacio Lopez</p>
            <p className="ion-text-left ion-no-margin"><strong>Correo</strong>: {'  '}contacto@accionfutbol.cl</p>   
          </IonCol>
        </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  )
}
