import './ExploreContainer.css';

interface ContainerProps {
  name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({ name }) => {
  return (
    <div className="container">
      <strong>Todo</strong>
      <p>settings</p>
      <p>media</p>
      <p>club board (home)</p>
      <p>club docs (pages)</p>
      <p>forms</p>
      <p>calendar (events)</p>
      <p>payment</p>
    </div>
  );
};

export default ExploreContainer;
