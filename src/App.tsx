import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

import {
  chatbubbles,
  grid,
  calendarClear,
  informationCircle
} from 'ionicons/icons';

import Home from './pages/Home';
import Messages from './pages/Messages';
// import Teams from './pages/Teams';
import Calendar from './pages/Calendar';
import Settings from './pages/Settings';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import { Chat } from 'pages/Chat/Chat';
import { NewMessage } from 'pages/Messages/NewMessage';
import ClubInfo from 'pages/ClubInfo';
import UserList from 'pages/UserList';
import UserDetails from 'pages/UserDetails';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/home">
            <Home />
          </Route>
          <Route exact path="/chat">
            <Messages />
          </Route>
          <Route exact path="/chat-new">
            <NewMessage />
          </Route>
          <Route exact path="/chat/:id">
            <Chat />
          </Route>
          {/* <Route path="/teams">
            <Teams />
          </Route> */}
          <Route path="/calendar">
            <Calendar />
          </Route>
          <Route exact path="/settings">
            <Settings />
          </Route>
          <Route exact path="/club-info">
            <ClubInfo />
          </Route>
          <Route exact path="/users">
            <UserList />
          </Route>
          <Route exact path="/users/:id">
            <UserDetails />
          </Route>
          <Route exact path="/">
            <Redirect to="/home" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="home" href="/home">
            <IonIcon icon={grid} />
            <IonLabel>Club</IonLabel>
          </IonTabButton>
          <IonTabButton tab="chat" href="/chat">
            <IonIcon icon={chatbubbles} />
            <IonLabel>Messages</IonLabel>
          </IonTabButton>
          {/* <IonTabButton tab="teams" href="/teams">
            <IonIcon icon={cube} />
            <IonLabel>Teams</IonLabel>
          </IonTabButton> */}
          <IonTabButton tab="calendar" href="/calendar">
            <IonIcon icon={calendarClear} />
            <IonLabel>Calendar</IonLabel>
          </IonTabButton>
          <IonTabButton tab="settings" href="/settings">
            <IonIcon icon={informationCircle} />
            <IonLabel>Settings</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
